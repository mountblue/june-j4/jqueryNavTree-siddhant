$(window).on("load", showSeasons);
$(window).on("load",showMatchesPerSeason);
//function to show seasons
function showSeasons() {
  $.ajax({
    type: "GET",
    url: "/api/seasons/season",
    dataType: "json",
    success: function (res) {
      if ($("#seasonContainer").children().length < 2) {
        for (i = 0; i < res.length; i++) {
          $("<p/>", {
            class: "year",
            id: res[i].season,
            text: res[i].season,
            onclick: "showTeamName(this)"
          }).appendTo("#seasonContainer").wrap("<div class='season'></div>");
        }
      }
    },
    error: function (res) {}
  });
}


//function to show team names
function showTeamName(event) {
  const year = event.innerText;
  $.ajax({
    type: "GET",
    url: "api/seasons/"+year+"/teams/" + year,
    dataType: "json",
    success: function (res) {
      if ($("#" + event.id).siblings().length < 1) {
        for (i = 0; i < res.length; i++) {
          $("<div/>", {
            class: "teamName",
            id: i,
            text: res[i].team,
            onclick: "showPlayerName("+year+",this)"
          }).appendTo($(event).parent()).wrap("<div class='teamContainer'></div>");
        }
        $(event).on("click").parent().siblings().children(".year").siblings().remove();
      } else {
        $(event).on("click").siblings().remove();
      }
    },
    error: function (res) {}
  });
}

function showPlayerName(year,event) {
  const team = event.innerText;
  $.ajax({
    type: "GET",
    url: "api/seasons/"+year+"/teams/players/" + team,
    dataType: "json",
    success: function (res) {
      if ($("#" + event.id).siblings().length < 1) {
        for (i = 0; i < res.length; i++) {
          $("<div/>", {
            class: "playerName",
            id: i,
            text: res[i].team,
            // onclick: "playerName(this)"
          }).appendTo(event).wrap("<div class='playerContainer'></div>");
        }
      } else {
        $(event).on("click").children().remove();
      }
    },
    error: function (res) {}
  });
}
//function to show matches per season
function showMatchesPerSeason() {
  $.ajax({
    type: "GET",
    url: "/api/seasons/matches",
    dataType: "json",
    success: function (res) {
      let chartType = "column";
      let chartTitle = "IPL Matches Per Season";
      let chartXaxis = {
        type: "category",
        labels: {
          rotation: -45,
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif"
          }
        }
      };

      let chartYaxis = {
        min: 0,
        title: {
          text: "Matches Per Season"
        }
      };

      let chartData = [{
        "colorByPoint": true,
        data: res,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: "#FFFFFF",
          align: "right",
          format: "{point.y:1f}",
          y: 10,
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif"
          }
        }
      }];
      let chartLegend = {
        enabled: false
      };
      let plotOptions = {
        series: {
          stacking: 'normal'
        }
      };
      plotGraph(chartType, chartTitle, chartXaxis, chartYaxis, chartData, chartLegend, plotOptions);
    },
    error: function (res) {}
  });
}
//---------------ploting graph for all charts------------------//

function plotGraph(chartType, chartTitle, chartXaxis, chartYaxis, chartData, chartLegend, plotOptions) {
  let chart = Highcharts.chart("container", {
    chart: {
      type: chartType
    },
    title: {
      text: chartTitle
    },
    xAxis: chartXaxis,
    yAxis: chartYaxis,
    legend: chartLegend,
    plotOptions: plotOptions,
    series: chartData,

  });
}