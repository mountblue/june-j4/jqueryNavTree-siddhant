const express = require("express");
const router = express.Router();
const iplData = require("../js/iplData");

//getting list of seasons
router.get("/seasons/season", function (req, res) {
    iplData.seasons().then(function (data) {
        res.send(data);
    });
});

//getting total number of matches per season
router.get("/seasons/matches/", function (req, res) {
    iplData.matchPerYear().then(function (data) {
        res.send(data);
    });
});

//getting teams name of the specific year
router.get("/seasons/:yr/teams/:yr", function (req, res) {
    iplData.teamName(req.params.yr).then(function (data) {
        console.log(data);
        res.send(data);
    });
});

//getting players of the specific team
router.get("/season/:yr/teams/players/:tm", function (req, res) {
    iplData.playerName(req.params.yr,req.params.tm).then(function (data) {
        res.send(data);
    });
});

module.exports = router;

//x/api/v1/seasons
//x/api/v1/seasons/2018
//x/api/v1/seasons/2018/teams
//x/api/v1/seasons/2018/teams/:team
//x/api/v1/seasons/2018/teams/:team/players
// x/api/v1/seasons/:season/teams/:team/players/:player