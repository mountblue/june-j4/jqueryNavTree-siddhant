//function to get seasons
function seasons() {
    const MongoClient = require("mongodb").MongoClient;
    const url = "mongodb://127.0.0.1:27017";
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log(err.message);
            } else {
                const dataSet = conn.db("dataSet");
                const matches = dataSet.collection("matches");
                matches.aggregate([{
                    $group: {
                        _id: "$season"
                    }
                }, {
                    $sort: {
                        _id: -1
                    }
                }, {
                    $project: {
                        _id: 0,
                        season: "$_id"
                    }
                }]).toArray(function (err, doc) {
                    resolve(doc);
                    conn.close();
                })

            }
        });
    });
}


//function to get name of the teams
function teamName(year) {
    const MongoClient = require("mongodb").MongoClient;
    const url = "mongodb://127.0.0.1:27017";
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log(err.message);
            } else {
                const dataSet = conn.db("dataSet");
                const matches = dataSet.collection("matches");
                matches.aggregate([{
                    $match: {
                        season: parseInt(year)
                    }
                }, {
                    $group: {
                        _id: "$team1"
                    }
                }, {
                    $project: {
                        _id: 0,
                        team: "$_id"
                    }
                }]).toArray(function (err, doc) {
                    resolve(doc);
                    conn.close();
                })

            }
        });
    });
}

//function to get players name
function playerName(year,tName) {
    const MongoClient = require("mongodb").MongoClient;
    const url = "mongodb://127.0.0.1:27017";
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log(err.message);
            } else {
                const dataSet = conn.db("dataSet");
                const matches = dataSet.collection("matches");
                matches.aggregate([{
                    $match: {
                        season:parseInt(year),
                        team1: tName
                    }
                }, {
                    $lookup: {
                        from: "deliveries",
                        localField: "id",
                        foreignField: "match_id",
                        as: "delivery"
                    }
                }, {
                    $unwind: "$delivery"
                }, {
                    $project: {
                        _id: 0,
                        delivery: {
                            batting_team: 1,
                            batsman: 1
                        }
                    }
                }, {
                    $group: {
                        _id: {
                            team: "$delivery.batting_team",
                            player: "$delivery.batsman"
                        }

                    }
                }, {
                    $match: {
                        "_id.team": tName
                    }
                },{$project:{
                    _id:0,
                    player:"$_id.player"
                }}]).toArray(function (err, doc) {
                    resolve(JSON.stringify(doc));
                    conn.close();
                })

            }
        });
    });
}

//function giving matches per year
function matchPerYear() {
    const MongoClient = require("mongodb").MongoClient;
    const url = "mongodb://127.0.0.1:27017";
    const fs = require("fs");
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, {
            useNewUrlParser: true
        }, function (err, conn) {
            if (err) {
                console.log(err.message);
                process.exit();
            } else {
                const dataSet = conn.db("dataSet");
                const matches = dataSet.collection("matches");
                matches.aggregate([{
                    $group: {
                        _id: "$season",
                        count: {
                            $sum: 1
                        }
                    }
                }, {
                    $sort: {
                        _id: -1
                    }
                }]).toArray(function (err, doc) {
                    var reformattedArray = doc.map(obj => {
                        var rObj = [];
                        rObj.push(obj._id, obj.count);
                        return rObj;
                    });
                    resolve(JSON.stringify(reformattedArray));
                    conn.close();
                })

            }
        });
    });
}

module.exports = {
    seasons: seasons,
    teamName: teamName,
    playerName:playerName,
    matchPerYear: matchPerYear
}