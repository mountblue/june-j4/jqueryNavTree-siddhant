const express = require("express");

//set up express app
const app = express();

app.use(express.static("public"));

app.get("/", function (req, res) {
    res.sendFile(__dirname + "/home.html");
})
//initialize routes
app.use("/api", require("./routes/api"));


//listen for requests
app.listen(3000, function () {
    console.log("now listening for requests");
});